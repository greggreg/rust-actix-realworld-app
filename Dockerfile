# # FROM alpine AS build
# # LABEL maintaner="greg@greggreg.org"
# # RUN apk add --update rust cargo
# # COPY . /app
# # WORKDIR /app
# # RUN cargo build
# # ENTRYPOINT ["cargo", "build", "--release"]

# # FROM alpine AS prod
# # WORKDIR /app
# # COPY --from=build /app/target/release/hello_docker .

# # Dockerfile for creating a statically-linked Rust application using docker's
# # multi-stage build feature. This also leverages the docker build cache to avoid
# # re-downloading dependencies if they have not changed.
# FROM rust:1.36.0 AS build
# WORKDIR /usr/src/rust-actix-realworld-app

# # Download the target for static linking.
# # RUN rustup target add x86_64-unknown-linux-musl

# # Create a dummy project and build the app's dependencies.
# # If the Cargo.toml or Cargo.lock files have not changed,
# # we can use the docker build cache and skip these (typically slow) steps.
# # RUN USER=root cargo new url-shortener
# # WORKDIR /usr/src/url-shortener
# COPY . .
# # COPY Cargo.toml Cargo.lock ./
# RUN cargo build --release

# # Copy the source and build the application.
# # COPY src ./src
# # RUN cargo install --target x86_64-unknown-linux-musl --path .
# RUN cargo install --path .
# RUN cargo clean

# # Copy the statically-linked binary into a new container.
# FROM debian:buster-slim

# COPY --from=build /usr/local/cargo/bin/rust-actix-realworld-app .
# COPY static static/
# COPY .env .env
# # Set user to first non-root user
# USER 1000

# CMD ["./rust-actix-realworld-app"]

# EXPOSE 8000
# -*- mode: dockerfile -*-
#
# An example Dockerfile showing how to build a Rust executable using this
# image, and deploy it with a tiny Alpine Linux container.

# You can override this `--build-arg BASE_IMAGE=...` to use different
# version of Rust or OpenSSL.

ARG BASE_IMAGE=ekidd/rust-musl-builder:stable

# Our first FROM statement declares the build environment.
FROM ${BASE_IMAGE} AS builder

# Add our source code.
ADD . ./

# Fix permissions on source code.
RUN sudo chown -R rust:rust /home/rust

# Build our application.
RUN cargo build --release

# Now, we need to build our _real_ Docker container, copying in `using-diesel`.
FROM alpine:latest
RUN apk --no-cache add ca-certificates
COPY --from=builder \
    /home/rust/src/target/x86_64-unknown-linux-musl/release/rust-actix-realworld-app \
    /app/
COPY static /app/static/
WORKDIR /app
EXPOSE 8000
CMD /app/rust-actix-realworld-app

