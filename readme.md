# ![RealWorld Example App](logo.png)

> ### [Actix](https://actix.rs/) and [Diesel](https://diesel.rs/) codebase containing real world examples (CRUD, auth, advanced patterns, etc) that adheres to the [RealWorld](https://github.com/gothinkster/realworld) spec and API.

### [Demo](https://github.com/gothinkster/realworld)&nbsp;&nbsp;&nbsp;&nbsp;[RealWorld](https://github.com/gothinkster/realworld)

This codebase was created to demonstrate a fully fledged fullstack application built with Actix and Diesel including CRUD operations, authentication, routing, pagination, and more.

We've gone to great lengths to adhere to the Actix and Diesel community styleguides & best practices.

For more information on how to this works with other frontends/backends, head over to the [RealWorld](https://github.com/gothinkster/realworld) repo.

# How it works

* [Actix](https://actix.rs/) for the web framework.
* [Diesel](https://diesel.rs/) for the database ORM.

And many other great rust libraries

* [chrono](https://crates.io/crates/chrono) for time stuff
* [serde](https://crates.io/crates/serde) for serialization / deserialization
* [validator](https://crates.io/crates/validator) for validating user input
* [slug](https://crates.io/crates/slug) for generating article slugs
* [jsonwebtoken](https://crates.io/crates/jsonwebtoken) for json web tokens

and many more, checkout cargo.toml for the full list.

# Try it out

The api is deployed here: http://li1283-241.members.linode.com/

Simply point any front end to that base url and give it a try!

# Getting started

* Install [Rust](https://www.rust-lang.org/tools/install)
* Install [Postgres](https://www.postgresql.org/download/)
  * on OS X with [homebrew](https://brew.sh/) `brew install postgresql`
* Install the Diesel Cli with postgres enabled
  * `cargo install diesel_cli --no-default-features --features postgres`
* Clone this repository
* Copy `.env.example` to `.env` and change the values if you like
* Create and migrate the database.
  * Run `diesel database setup` from project root.
  * it should say `Creating database: actix_realworld` and then run some migrations.
  * If this failed, make sure postgres is running and make sure `DATABASE_URL` in `.env` is correct for your setup.
* Build and run the application with `cargo run`
  * If build succeeds the application will be running on port `8000`
  * If you would like to set up hot code reloading see below.

# Hot Code Reloading

When developing it can be useful to have the application automatically recompile when any files changes.
To enable this:

* install [systemfd](https://github.com/mitsuhiko/systemfd) and [cargo-watch](https://github.com/passcod/cargo-watch)
  * `cargo install systemfd cargo-watch`
* rather than using `cargo run` to run the application use:
  * `systemfd --no-pid -s http::8000 -- cargo watch -x run`

# Deploy

The application is fully dockerized

* copy `.env.example` to `.env.prod` and fill in the appropriate values
* make sure the postgres env vars in `docker-compose.yml` are what you would like
* run `docker-compose up`
