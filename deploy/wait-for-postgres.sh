#!/bin/sh
# wait-for-postgres.sh

set -e

until diesel database setup; do
  >&2 echo "Postgres is unavailable - sleeping"
  sleep 1
done

>&2 echo "Postgres is up - executing command"
diesel migration run

