CREATE TABLE comments (
    article_id UUID NOT NULL REFERENCES articles (id),
    author_id UUID NOT NULL REFERENCES users (id),
    body TEXT NOT NULL,
    created_at TIMESTAMPTZ DEFAULT CURRENT_TIMESTAMP NOT NULL,
    id UUID PRIMARY KEY DEFAULT uuid_generate_v4(),
    updated_at TIMESTAMPTZ DEFAULT CURRENT_TIMESTAMP NOT NULL
);

CREATE INDEX comments_author_id_idx ON comments (author_id);
CREATE INDEX comments_article_id_idx ON comments (author_id);
SELECT diesel_manage_updated_at('comments');


