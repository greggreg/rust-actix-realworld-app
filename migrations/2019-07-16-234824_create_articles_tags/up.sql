CREATE TABLE article_tags (
    PRIMARY KEY (article_id, label),
    article_id UUID NOT NULL REFERENCES articles (id),
    created_at TIMESTAMPTZ DEFAULT CURRENT_TIMESTAMP NOT NULL,
    label TEXT NOT NULL,
    updated_at TIMESTAMPTZ DEFAULT CURRENT_TIMESTAMP NOT NULL
);

CREATE INDEX article_tags_article_id_idx ON article_tags (article_id);
CREATE INDEX article_tags_tag_name_idx ON article_tags (label);

SELECT diesel_manage_updated_at('article_tags');


