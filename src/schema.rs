table! {
    article_tags (article_id, label) {
        article_id -> Uuid,
        created_at -> Timestamptz,
        label -> Text,
        updated_at -> Timestamptz,
    }
}

table! {
    articles (id) {
        author_id -> Uuid,
        body -> Text,
        created_at -> Timestamptz,
        description -> Text,
        favorited_by -> Array<Uuid>,
        id -> Uuid,
        slug -> Text,
        title -> Text,
        updated_at -> Timestamptz,
    }
}

table! {
    comments (id) {
        article_id -> Uuid,
        author_id -> Uuid,
        body -> Text,
        created_at -> Timestamptz,
        id -> Uuid,
        updated_at -> Timestamptz,
    }
}

table! {
    users (id) {
        bio -> Text,
        created_at -> Timestamptz,
        email -> Varchar,
        following -> Array<Uuid>,
        id -> Uuid,
        image -> Nullable<Varchar>,
        password_hash -> Varchar,
        updated_at -> Timestamptz,
        username -> Varchar,
    }
}

joinable!(article_tags -> articles (article_id));
joinable!(articles -> users (author_id));
joinable!(comments -> articles (article_id));
joinable!(comments -> users (author_id));

allow_tables_to_appear_in_same_query!(
    article_tags,
    articles,
    comments,
    users,
);
