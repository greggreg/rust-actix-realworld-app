//
// This Middleware parses and validates a JWT if the Authorization header is present.
// If the header is not present this middleware moves on to the next middleware.
//
use crate::api::LoggedInUserId;
use crate::error::Error::AuthError;
use crate::jwt;
use actix_service::{Service, Transform};
use actix_web::dev::{ServiceRequest, ServiceResponse};
use actix_web::error::ResponseError;
use actix_web::http::header::{HeaderValue, AUTHORIZATION};
use actix_web::web::Data;
use actix_web::{Error as ActixError, HttpMessage};
use futures::future::{ok, FutureResult};
use futures::{Future, Poll};
// There are two steps in middleware processing.
// 1. Middleware initialization, middleware factory gets called with
//    next service in chain as parameter.
// 2. Middleware's call method gets called with normal request.
pub struct JwtAuth;

// Middleware factory is `Transform` trait from actix-service crate
// `S` - type of the next service
// `B` - type of response's body
impl<S, B> Transform<S> for JwtAuth
where
    S: Service<Request = ServiceRequest, Response = ServiceResponse<B>, Error = ActixError>,
    S::Future: 'static,
    B: 'static,
{
    type Request = ServiceRequest;
    type Response = ServiceResponse<B>;
    type Error = ActixError;
    type InitError = ();
    type Transform = JwtAuthMiddleware<S>;
    type Future = FutureResult<Self::Transform, Self::InitError>;

    fn new_transform(&self, service: S) -> Self::Future {
        ok(JwtAuthMiddleware { service })
    }
}

pub struct JwtAuthMiddleware<S> {
    service: S,
}

impl<S, B> Service for JwtAuthMiddleware<S>
where
    S: Service<Request = ServiceRequest, Response = ServiceResponse<B>, Error = ActixError>,
    S::Future: 'static,
    B: 'static,
{
    type Request = ServiceRequest;
    type Response = ServiceResponse<B>;
    type Error = ActixError;
    type Future = Box<Future<Item = Self::Response, Error = ActixError>>;

    fn poll_ready(&mut self) -> Poll<(), Self::Error> {
        self.service.poll_ready()
    }

    fn call(&mut self, req: ServiceRequest) -> Self::Future {
        let jwt_config: Option<Data<jwt::Config>> = req.app_data();
        let auth_header: Option<&HeaderValue> = req.headers().get(AUTHORIZATION);
        let auth_header_val: Option<String> = match auth_header {
            Some(header) => match header.to_str() {
                Ok(str) => Some(str.to_string()),
                Err(_) => None,
            },
            None => None,
        };

        match (jwt_config, auth_header_val) {
            (Some(config), Some(val)) => {
                let jwt_string = val.trim_start_matches("Token ");
                match crate::jwt::verify_user(&config, jwt_string) {
                    Ok(user_id) => {
                        req.extensions_mut().insert(LoggedInUserId(user_id));
                        Box::new(self.service.call(req).and_then(Ok))
                    }

                    Err(_) => Box::new(ok(req.error_response(
                        AuthError("JWT not decoded successfully.".to_string()).error_response(),
                    ))),
                }
            }

            _ => Box::new(self.service.call(req).and_then(Ok)),
        }
    }
}
