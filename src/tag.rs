use crate::error;
use crate::schema::article_tags;
use crate::schema::article_tags::dsl::{
    article_id as column_article_id, article_tags as tags_table, label as column_label,
};
use crate::DbPool;
use diesel::prelude::RunQueryDsl;
use diesel::query_dsl::QueryDsl;
use diesel::ExpressionMethods;

#[derive(Debug, Insertable, Queryable)]
#[table_name = "article_tags"]
pub struct TagInDatabase {
    pub article_id: uuid::Uuid,
    pub created_at: chrono::DateTime<chrono::Utc>,
    pub label: String,
    pub updated_at: chrono::DateTime<chrono::Utc>,
}

impl TagInDatabase {
    fn into_output(self) -> TagOutput {
        self.label
    }
}

#[derive(Debug, Insertable)]
#[table_name = "article_tags"]
pub struct NewTag {
    pub label: String,
    pub article_id: uuid::Uuid,
}

impl NewTag {
    pub fn into_output(self) -> TagOutput {
        self.label
    }
}

pub type TagOutput = String;

pub fn process_tags_input(mut input: Vec<String>) -> Vec<String> {
    let mut uniques = std::collections::HashSet::new();
    input.retain(|e| uniques.insert(e.clone()));
    input.into_iter().map(|t| t.to_lowercase()).collect()
}

pub fn get_all(pool: &DbPool) -> Result<Vec<TagOutput>, error::Error> {
    let conn = &pool.get()?;
    let tags = tags_table
        .distinct_on(column_label)
        .load::<TagInDatabase>(conn)?
        .into_iter()
        .map(|t| t.into_output())
        .collect();

    Ok(tags)
}

pub fn get_by_label(pool: &DbPool, label: &str) -> Result<Vec<TagInDatabase>, error::Error> {
    let conn = &pool.get()?;

    let tag = tags_table
        .filter(column_label.eq(label))
        .get_results::<TagInDatabase>(conn)?;

    Ok(tag)
}

pub fn get_for_article(
    pool: &DbPool,
    article_id: &uuid::Uuid,
) -> Result<Vec<String>, crate::error::Error> {
    let conn = &pool.get()?;

    let tags = tags_table
        .filter(column_article_id.eq(article_id))
        .get_results::<TagInDatabase>(conn)?;

    Ok(tags.into_iter().map(|t| t.into_output()).collect())
}

pub fn insert_tags_for_article(
    pool: &DbPool,
    article_id: uuid::Uuid,
    tags: &[String],
) -> Result<usize, crate::error::Error> {
    let conn = &pool.get()?;
    let new_tags: Vec<NewTag> = tags
        .iter()
        .map(|t| NewTag {
            label: t.to_string(),
            article_id,
        })
        .collect();
    diesel::insert_into(tags_table)
        .values(new_tags)
        .on_conflict((column_label, column_article_id))
        .do_nothing()
        .execute(conn)
        .map_err(Into::into)
}

pub fn delete_all_tags_for_article(
    pool: &DbPool,
    article_id: uuid::Uuid,
) -> Result<usize, crate::error::Error> {
    let conn = &pool.get()?;
    diesel::delete(tags_table.filter(column_article_id.eq(article_id)))
        .execute(conn)
        .map_err(Into::into)
}
