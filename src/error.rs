///
/// This module wraps all the types of errors this application and its libraries can produce into
/// one convenient type. This way we can use the ? operator on any result producing function call
/// and have it return an error in our type and similarly we can call `.into()` on any error type
/// to have it turned into this type.
///
/// With this central error type we can also choose how to respond to a client when our application
/// errors in one convenient location.
///
use diesel::result::{DatabaseErrorKind, Error as DieselError};
use serde_json::{Map as JsonMap, Value as JsonValue};

/// Our Applications error type that wraps all other error types.
#[derive(Debug, Fail)]
pub enum Error {
    #[fail(display = "Auth Error: {}", _0)]
    AuthError(String),

    #[fail(display = "Argon2 Error {}", _0)]
    Argon2Error(argon2::Error),

    #[fail(display = "Diesel Error ")]
    DieselError(diesel::result::Error),

    #[fail(display = "Jwt Encode Errror: {}", _0)]
    JwtError(jsonwebtoken::errors::Error),

    #[fail(display = "Login Error")]
    LoginError,

    #[fail(display = "r2d2 Error")]
    R2d2Error(r2d2::Error),

    #[fail(display = "Uuid Error{}", _0)]
    UuidError(uuid::Error),

    #[fail(display = "Validation Error {}", _0)]
    ValidationErrors(validator::ValidationErrors),
}

/// Impl of ResponseError for our Error type so we can choose how to correctly respond
/// in case of any error.
impl actix_web::error::ResponseError for Error {
    fn error_response(&self) -> actix_web::HttpResponse {
        match self {
            Error::AuthError(ref message) => unauthorized_response(message),

            Error::Argon2Error(ref err) => internal_server_error_response(err),

            Error::DieselError(ref err) => match err {
                DieselError::DatabaseError(kind, info) => match kind {
                    DatabaseErrorKind::UniqueViolation => {
                        let json = json!({
                            "errors": {
                                constraint_name_to_field_name(info.constraint_name()) :
                                    "has already been taken"
                            }
                        });
                        unprocessable_entity_response(&json)
                    }

                    DatabaseErrorKind::ForeignKeyViolation => {
                        let error = match info.column_name() {
                            Some(name) => json!({ "errors": { name: info.message()} }),
                            None => json!({ "error": info.message() }),
                        };
                        unprocessable_entity_response(&error)
                    }

                    _ => internal_server_error_response(&self),
                },

                DieselError::NotFound => not_found_response(),

                _ => internal_server_error_response(&self),
            },

            Error::JwtError(ref err) => unauthorized_response(&format!("{:?}", err)),

            Error::LoginError => {
                let error = json!({"errors": { "email or password" : ["is invalid"]}});
                unprocessable_entity_response(&error)
            }

            Error::R2d2Error(ref err) => internal_server_error_response(err),

            Error::UuidError(ref _err) => {
                let error = json!({"error": "unvalid uuid"});
                unprocessable_entity_response(&error)
            }

            Error::ValidationErrors(ref errs) => {
                unprocessable_entity_response(&validation_errs_to_json(errs))
            }
        }
    }
}

impl From<argon2::Error> for Error {
    fn from(error: argon2::Error) -> Self {
        Error::Argon2Error(error)
    }
}

impl From<diesel::result::Error> for Error {
    fn from(errors: diesel::result::Error) -> Self {
        Error::DieselError(errors)
    }
}
impl From<jsonwebtoken::errors::Error> for Error {
    fn from(error: jsonwebtoken::errors::Error) -> Self {
        Error::JwtError(error)
    }
}

impl From<uuid::Error> for Error {
    fn from(error: uuid::Error) -> Self {
        Error::UuidError(error)
    }
}

impl From<r2d2::Error> for Error {
    fn from(errors: r2d2::Error) -> Self {
        Error::R2d2Error(errors)
    }
}
impl From<validator::ValidationErrors> for Error {
    fn from(errors: validator::ValidationErrors) -> Self {
        Error::ValidationErrors(errors)
    }
}

fn validation_errs_to_json(errors: &validator::ValidationErrors) -> JsonValue {
    let mut err_map = JsonMap::new();

    for (field, errors) in errors.clone().field_errors().iter() {
        let errors: Vec<JsonValue> = errors.iter().map(|error| json!(error.message)).collect();
        err_map.insert(field.to_string(), json!(errors));
    }

    json!({ "errors": err_map })
}

fn constraint_name_to_field_name(constraint_name: Option<&str>) -> String {
    match constraint_name {
        Some("users_email_key") => "email".to_string(),

        Some("users_username_key") => "username".to_string(),

        Some(name) => name.to_string(),

        None => "".to_string(),
    }
}

fn unprocessable_entity_response(json: &serde_json::Value) -> actix_web::HttpResponse {
    actix_web::HttpResponse::build(actix_web::http::StatusCode::UNPROCESSABLE_ENTITY).json(json)
}

fn internal_server_error_response<T>(err: T) -> actix_web::HttpResponse
where
    T: std::fmt::Debug,
{
    println!("INTERNAL SERVER ERROR : {:?}", err);
    actix_web::HttpResponse::InternalServerError().json(json!({"error": "Internal Server Error"}))
}

fn not_found_response() -> actix_web::HttpResponse {
    actix_web::HttpResponse::NotFound().json(json!({"error": "not found"}))
}

fn unauthorized_response(message: &str) -> actix_web::HttpResponse {
    actix_web::HttpResponse::Unauthorized().json(json!({ "error": message }))
}
