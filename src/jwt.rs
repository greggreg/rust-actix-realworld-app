#[derive(Debug, Clone)]
pub struct Config {
    pub secret: String,
}

#[derive(Debug, Serialize, Deserialize)]
struct Claims {
    #[serde(with = "chrono::serde::ts_seconds")]
    exp: chrono::DateTime<chrono::Utc>,
    user_id: uuid::Uuid,
}

pub fn encode_for_user(
    config: &Config,
    user_id: &uuid::Uuid,
) -> Result<String, crate::error::Error> {
    let claims = Claims {
        exp: chrono::Utc::now() + chrono::Duration::days(10),
        user_id: *user_id,
    };
    let token = jsonwebtoken::encode(
        &jsonwebtoken::Header::default(),
        &claims,
        config.secret.as_ref(),
    )?;
    Ok(token)
}

pub fn verify_user(config: &Config, token: &str) -> Result<uuid::Uuid, crate::error::Error> {
    let token_data = jsonwebtoken::decode::<Claims>(
        token,
        config.secret.as_ref(),
        &jsonwebtoken::Validation::default(),
    )?;
    Ok(token_data.claims.user_id)
}
