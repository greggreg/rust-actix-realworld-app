use crate::diesel::{ExpressionMethods, QueryDsl};
use crate::error::Error;
use crate::jwt;
use crate::schema::users;
use crate::schema::users::dsl::{
    email as column_email, following as column_following, id as column_id,
    username as column_username, users as users_table,
};
use crate::DbPool;
use diesel::prelude::RunQueryDsl;
use diesel::result::Error as DieselError;
use validator::{Validate, ValidationError, Validator};

const DEFAULT_USER_ICON: &str =
    "https://glcdn.githack.com/greggreg/rust-actix-realworld-app/raw/master/static/user_icon.png";

/*
 * INPUT STRUCTS
 */

// This struct represents the user input for creating a new user
// It can come from any external source, but this app only supports
// decoding from JSON. We use #[serde(default)] because we want the validation
// errors even if the fields haven't been specified
#[derive(Debug, Deserialize, Validate)]
pub struct NewUserInput {
    #[serde(default)]
    #[validate(email(message = "must be a valid email"))]
    email: String,

    #[serde(default)]
    #[validate(custom = "validate_username")]
    username: String,

    #[serde(default)]
    #[validate(custom = "validate_password")]
    password: String,
}

// This struct represents the user input for updating a user
// All fields are Option<_> because none are required on update.
#[derive(Debug, Deserialize, Validate)]
pub struct UpdateUserInput {
    bio: Option<String>,

    #[validate(email(message = "must be a valid email"))]
    email: Option<String>,

    image: Option<String>,

    #[validate(custom = "validate_username")]
    username: Option<String>,

    #[validate(custom = "validate_password")]
    password: Option<String>,
}

impl UpdateUserInput {
    fn into_update_user(self) -> Result<UpdateUser, crate::error::Error> {
        self.validate()?;
        let mut password_hash = None;
        if let Some(password) = self.password {
            password_hash = Some(hash_pw(&password)?);
        };

        Ok(UpdateUser {
            bio: self.bio,
            email: self.email,
            image: self.image,
            password_hash,
            username: self.username,
        })
    }
}

#[derive(Debug, Deserialize)]
pub struct UserLoginInput {
    #[serde(default)]
    email: String,

    #[serde(default)]
    password: String,
}

/*
 * INPUT VALIDATIONS
*/

fn validate_username(username: &str) -> Result<(), ValidationError> {
    let v = Validator::Length {
        min: Some(1),
        max: Some(50),
        equal: None,
    };
    if validator::validate_length(v, username) {
        Ok(())
    } else {
        Err(ValidationError::new(
            "must be between 1 and 50 characters long",
        ))
    }
}

fn validate_password(password: &str) -> Result<(), ValidationError> {
    let v = Validator::Length {
        min: Some(8),
        max: Some(72),
        equal: None,
    };
    if validator::validate_length(v, password) {
        Ok(())
    } else {
        Err(ValidationError::new(
            "must be between 8 and 72 characters long",
        ))
    }
}

/*
 * OUTPUT STRUCTS
 */

// This struct represents a user that can be delivered
// outside the application. Notice it is missing fields
// like password_hash and has extra fields like token
#[derive(Debug, Serialize)]
pub struct UserOutput {
    pub bio: String,
    pub email: String,
    pub image: Option<String>,
    pub token: String,
    pub username: String,
}

#[derive(Debug, Serialize)]
pub struct AuthorOutput {
    pub username: String,
    pub bio: String,
    pub image: Option<String>,
    pub following: bool,
}

#[derive(Debug, Serialize)]
pub struct UserProfileOutput {
    bio: String,
    following: bool,
    image: Option<String>,
    username: String,
}

/*
 * APPLICATION STRUCTS
 */

#[derive(Debug, AsChangeset)]
#[table_name = "users"]
struct UpdateUser {
    bio: Option<String>,
    email: Option<String>,
    image: Option<String>,
    password_hash: Option<String>,
    username: Option<String>,
}

// This struct represents a User that can actually be
// inserted into the database
#[derive(Debug, Insertable)]
#[table_name = "users"]
struct NewUser {
    email: String,
    following: Vec<uuid::Uuid>,
    password_hash: String,
    username: String,
}

// This struct represents the user as it exists in the database.
#[derive(Debug, Queryable, Identifiable)]
#[table_name = "users"]
pub struct UserInDatabase {
    bio: String,
    created_at: chrono::DateTime<chrono::Utc>,
    email: String,
    pub following: Vec<uuid::Uuid>,
    pub id: uuid::Uuid,
    image: Option<String>,
    password_hash: String,
    updated_at: chrono::DateTime<chrono::Utc>,
    username: String,
}

impl UserInDatabase {
    fn into_output(self, jwt_config: &jwt::Config) -> Result<UserOutput, Error> {
        let token = crate::jwt::encode_for_user(jwt_config, &self.id)?;
        let image = self.image.or_else(|| Some(DEFAULT_USER_ICON.to_string()));

        Ok(UserOutput {
            bio: self.bio,
            email: self.email,
            image,
            token,
            username: self.username,
        })
    }

    fn into_profile(self, following: bool) -> UserProfileOutput {
        let image = self.image.or_else(|| Some(DEFAULT_USER_ICON.to_string()));

        UserProfileOutput {
            bio: self.bio,
            following,
            image,
            username: self.username,
        }
    }

    pub fn into_author_output(self, following: bool) -> AuthorOutput {
        let image = self.image.or_else(|| Some(DEFAULT_USER_ICON.to_string()));
        AuthorOutput {
            username: self.username,
            bio: self.bio,
            image,
            following,
        }
    }

    pub fn is_following(&self, id: &uuid::Uuid) -> bool {
        self.following.contains(id)
    }
}

// This type affords easy validaion of NewUserInput to NewUser
// Ok(_) means good to insert!
type ValidatedNewUser = Result<NewUser, Error>;
impl std::convert::From<NewUserInput> for ValidatedNewUser {
    fn from(input: NewUserInput) -> Self {
        input.validate()?;
        let password_hash = hash_pw(&input.password)?;
        Ok(NewUser {
            email: input.email,
            following: vec![],
            password_hash,
            username: input.username,
        })
    }
}

pub fn create_user(
    pool: &DbPool,
    jwt_config: &jwt::Config,
    new_user_input: NewUserInput,
) -> Result<UserOutput, Error> {
    let new_user = ValidatedNewUser::from(new_user_input)?;
    let user = persist_new_user(pool, jwt_config, new_user)?;
    Ok(user)
}

pub fn update_user(
    pool: &DbPool,
    jwt_config: &jwt::Config,
    user_id: uuid::Uuid,
    update_user_input: UpdateUserInput,
) -> Result<UserOutput, Error> {
    let updated_user = update_user_input.into_update_user()?;
    let user = persist_updated_user(pool, jwt_config, user_id, updated_user)?;
    Ok(user)
}

pub fn login(
    pool: &DbPool,
    jwt_config: &jwt::Config,
    user_login_input: UserLoginInput,
) -> Result<UserOutput, Error> {
    match get_user_by_email(pool, &user_login_input.email) {
        Err(Error::DieselError(DieselError::NotFound)) => Err(Error::LoginError),

        Err(err) => Err(err),

        Ok(user) => {
            let verified = verify_pw(&user_login_input.password, &user.password_hash)?;
            if verified {
                user.into_output(jwt_config)
            } else {
                Err(Error::LoginError)
            }
        }
    }
}

#[derive(Debug)]
pub enum FollowOperation {
    Follow,
    Unfollow,
}

pub fn get_author_by_id(
    pool: &DbPool,
    user_id: uuid::Uuid,
    logged_in_user_id: Option<uuid::Uuid>,
) -> Result<AuthorOutput, Error> {
    let conn = &pool.get()?;
    let user = users_table.find(user_id).first::<UserInDatabase>(conn)?;
    let mut following = false;
    if let Some(logged_in_user_id) = logged_in_user_id {
        let is_following = users_table
            .find(logged_in_user_id)
            .select(column_following)
            .get_result::<Vec<uuid::Uuid>>(conn)?;

        following = is_following.contains(&user.id);
    }

    Ok(user.into_author_output(following))
}

pub fn get(pool: &DbPool, user_id: &uuid::Uuid) -> Result<UserInDatabase, Error> {
    let conn = &pool.get()?;
    users_table.find(&user_id).first(conn).map_err(|e| e.into())
}

pub fn get_user_by_id(
    pool: &DbPool,
    jwt_config: &jwt::Config,
    user_id: uuid::Uuid,
) -> Result<UserOutput, Error> {
    let conn = &pool.get()?;
    let user = users_table.find(user_id).first::<UserInDatabase>(conn)?;
    user.into_output(jwt_config)
}

pub fn get_following(pool: &DbPool, user_id: &uuid::Uuid) -> Result<Vec<uuid::Uuid>, Error> {
    let conn = &pool.get()?;
    let following = users_table
        .find(user_id)
        .select(column_following)
        .get_result::<Vec<uuid::Uuid>>(conn)?;
    Ok(following)
}

pub fn get_id_by_username(pool: &DbPool, username: &str) -> Result<uuid::Uuid, Error> {
    let conn = &pool.get()?;

    let id: uuid::Uuid = users_table
        .filter(column_username.eq(username))
        .select(column_id)
        .get_result(conn)?;

    Ok(id)
}

pub fn get_profile_for_username(
    pool: &DbPool,
    username: &str,
    logged_in_user: Option<uuid::Uuid>,
) -> Result<UserProfileOutput, Error> {
    let conn = &pool.get()?;

    let user = users_table
        .filter(column_username.eq(username))
        .first::<UserInDatabase>(conn)?;

    let mut following = false;

    if let Some(user_id) = logged_in_user {
        let users_following = users_table
            .find(user_id)
            .select(column_following)
            .get_result::<Vec<uuid::Uuid>>(conn)?;
        following = users_following.contains(&user.id);
    }

    Ok(user.into_profile(following))
}

pub fn manage_follow(
    pool: &DbPool,
    follower: uuid::Uuid,
    followed: String,
    follow_operation: FollowOperation,
) -> Result<UserProfileOutput, Error> {
    let conn = &pool.get()?;

    let mut currently_following: Vec<uuid::Uuid> = users_table
        .find(&follower)
        .select(column_following)
        .first::<Vec<uuid::Uuid>>(conn)?;

    let user_to_follow = users_table
        .filter(column_username.eq(&followed))
        .select(column_id)
        .first::<uuid::Uuid>(conn)?;

    match follow_operation {
        FollowOperation::Follow => {
            if !currently_following.contains(&user_to_follow) {
                currently_following.push(user_to_follow);
            }
        }
        FollowOperation::Unfollow => {
            let index = currently_following
                .iter()
                .position(|u| *u == user_to_follow);
            if let Some(i) = index {
                currently_following.remove(i);
            }
        }
    }

    let following = currently_following.contains(&user_to_follow);

    let profile = diesel::update(users_table.find(follower))
        .set(column_following.eq(currently_following))
        .get_result::<UserInDatabase>(conn)?;

    Ok(profile.into_profile(following))
}

fn persist_new_user(
    pool: &DbPool,
    jwt_config: &jwt::Config,
    new_user: NewUser,
) -> Result<UserOutput, Error> {
    let conn = &pool.get()?;
    let user = diesel::insert_into(users_table)
        .values(new_user)
        .get_result::<UserInDatabase>(conn)?;

    user.into_output(jwt_config)
}

fn persist_updated_user(
    pool: &DbPool,
    jwt_config: &jwt::Config,
    user_id: uuid::Uuid,
    update_user: UpdateUser,
) -> Result<UserOutput, Error> {
    let conn = &pool.get()?;

    let user = diesel::update(users_table.find(user_id))
        .set(update_user)
        .get_result::<UserInDatabase>(conn)?;

    user.into_output(jwt_config)
}

fn get_user_by_email(pool: &DbPool, email: &str) -> Result<UserInDatabase, Error> {
    let conn = &pool.get()?;
    let user = users_table
        .filter(column_email.eq(email))
        .get_result::<UserInDatabase>(conn)?;
    Ok(user)
}

pub fn get_by_username(pool: &DbPool, username: &str) -> Result<UserInDatabase, Error> {
    let conn = &pool.get()?;
    let user = users_table
        .filter(column_username.eq(username))
        .get_result::<UserInDatabase>(conn)?;
    Ok(user)
}

fn hash_pw(input: &str) -> Result<String, crate::error::Error> {
    let salt = uuid::Uuid::new_v4().to_string();

    argon2::hash_encoded(
        input.as_bytes(),
        salt.as_bytes(),
        &argon2::Config::default(),
    )
    .map_err(|e| e.into())
}

fn verify_pw(input: &str, hash: &str) -> Result<bool, crate::error::Error> {
    argon2::verify_encoded(&hash, input.as_bytes()).map_err(|e| e.into())
}
