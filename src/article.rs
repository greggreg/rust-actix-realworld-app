use crate::diesel::{ExpressionMethods, PgArrayExpressionMethods, QueryDsl};
use crate::error;
use crate::schema::articles;
use crate::schema::articles::dsl::{
    articles as articles_table, author_id as column_author_id, created_at as column_created_at,
    favorited_by as column_favorited_by, id as column_id, slug as column_slug,
};
use crate::schema::users::dsl::users as users_table;
use crate::user::UserInDatabase;
use crate::DbPool;
use diesel::prelude::RunQueryDsl;
use validator::Validate;

/*
 * INPUT STRUCTS
 */

#[derive(Debug, Deserialize, Validate)]
#[serde(rename_all = "camelCase")]
pub struct ArticleInput {
    #[validate(length(min = "1", message = "cannot be empty"))]
    #[serde(default)]
    title: String,

    #[validate(length(min = "1", message = "cannot be empty"))]
    #[serde(default)]
    description: String,

    #[validate(length(min = "1", message = "cannot be empty"))]
    #[serde(default)]
    body: String,

    #[serde(default)]
    tag_list: Vec<String>,
}

impl ArticleInput {
    fn into_new_article(
        self,
        user_id: &uuid::Uuid,
    ) -> Result<(NewArticle, Vec<String>), crate::error::Error> {
        self.validate()?;
        let tags = crate::tag::process_tags_input(self.tag_list);
        Ok((
            NewArticle {
                author_id: *user_id,
                body: self.body,
                description: self.description,
                favorited_by: vec![],
                slug: slug::slugify(&self.title),
                title: self.title,
            },
            tags,
        ))
    }

    fn into_update_article(self) -> Result<(UpdateArticle, Vec<String>), crate::error::Error> {
        let tags = crate::tag::process_tags_input(self.tag_list);
        Ok((
            UpdateArticle {
                body: self.body,
                description: self.description,
                slug: slug::slugify(&self.title),
                title: self.title,
            },
            tags,
        ))
    }
}

/*
 * OUTPUT STRUCTS
 */

#[derive(Debug, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct ArticleOutput {
    author: crate::user::AuthorOutput,
    body: String,
    created_at: chrono::DateTime<chrono::Utc>,
    description: String,
    favorited: bool,
    favorites_count: usize,
    slug: String,
    tag_list: Vec<crate::tag::TagOutput>,
    title: String,
    updated_at: chrono::DateTime<chrono::Utc>,
}

/*
 * APPLICATION STRUCTS
 */

#[derive(Debug, Insertable, Queryable, Serialize)]
#[table_name = "articles"]
pub struct ArticleInDatabase {
    pub author_id: uuid::Uuid,
    pub body: String,
    pub created_at: chrono::DateTime<chrono::Utc>,
    pub description: String,
    pub favorited_by: Vec<uuid::Uuid>,
    pub id: uuid::Uuid,
    pub slug: String,
    pub title: String,
    pub updated_at: chrono::DateTime<chrono::Utc>,
}

impl ArticleInDatabase {
    fn into_output(
        self,
        author: crate::user::AuthorOutput,
        tag_list: Vec<crate::tag::TagOutput>,
        logged_in_user_id: &Option<uuid::Uuid>,
    ) -> ArticleOutput {
        let favorited = logged_in_user_id
            .map(|id| self.favorited_by.contains(&id))
            .unwrap_or(false);
        ArticleOutput {
            author,
            body: self.body,
            created_at: self.created_at,
            description: self.description,
            favorited,
            favorites_count: self.favorited_by.len(),
            slug: self.slug,
            tag_list,
            title: self.title,
            updated_at: self.updated_at,
        }
    }
}

#[derive(Debug, Insertable)]
#[table_name = "articles"]
struct NewArticle {
    pub author_id: uuid::Uuid,
    pub body: String,
    pub description: String,
    pub favorited_by: Vec<uuid::Uuid>,
    pub slug: String,
    pub title: String,
}

#[derive(Debug, AsChangeset)]
#[table_name = "articles"]
struct UpdateArticle {
    pub body: String,
    pub description: String,
    pub slug: String,
    pub title: String,
}

#[derive(Debug, Deserialize)]
pub struct SearchFilters {
    pub author: Option<String>,
    pub authors: Option<Vec<uuid::Uuid>>,
    pub favorited: Option<String>,
    pub limit: Option<i64>,
    pub offset: Option<i64>,
    pub tag: Option<String>,
}

pub fn create_article(
    pool: &DbPool,
    logged_in_user_id: uuid::Uuid,
    new_article_input: ArticleInput,
) -> Result<ArticleOutput, crate::error::Error> {
    let conn = &pool.get()?;
    let (new_article, tags) = new_article_input.into_new_article(&logged_in_user_id)?;

    let article = diesel::insert_into(articles_table)
        .values(new_article)
        .get_result::<ArticleInDatabase>(conn)?;

    let author = crate::user::get_author_by_id(pool, logged_in_user_id, None)?;

    let _ = crate::tag::insert_tags_for_article(pool, article.id, &tags);

    Ok(article.into_output(author, tags, &Some(logged_in_user_id)))
}

pub fn get_all(
    pool: &DbPool,
    filters: &SearchFilters,
    logged_in_user_id: Option<uuid::Uuid>,
) -> Result<(Vec<ArticleOutput>, usize), error::Error> {
    let conn = pool.get()?;

    let mut ids_for_tag: Option<Vec<uuid::Uuid>> = None;
    if let Some(tag) = &filters.tag {
        if let Ok(tags) = crate::tag::get_by_label(pool, &tag) {
            ids_for_tag = Some(tags.into_iter().map(|t| t.article_id).collect());
        }
    }

    let mut author_id: Option<uuid::Uuid> = None;
    if let Some(author) = &filters.author {
        if let Ok(user) = crate::user::get_by_username(pool, author) {
            author_id = Some(user.id);
        }
    }

    let mut query = articles_table.inner_join(users_table).into_boxed();
    let mut count_query = articles_table.into_boxed();

    if let Some(ids) = ids_for_tag {
        query = query.filter(column_id.eq_any(ids.clone()));
        count_query = count_query.filter(column_id.eq_any(ids));
    }

    if let Some(authors) = &filters.authors {
        query = query.filter(column_author_id.eq_any(authors.clone()));
        query = query.filter(column_author_id.eq_any(authors));
    } else if let Some(id) = &author_id {
        query = query.filter(column_author_id.eq(id.clone()));
        count_query = count_query.filter(column_author_id.eq(id));
    }

    if let Some(favoriter) = &filters.favorited {
        let mut ids: Vec<uuid::Uuid> = vec![];
        if let Ok(id) = crate::user::get_id_by_username(pool, favoriter) {
            ids.push(id)
        }
        query = query.filter(column_favorited_by.contains(ids.clone()));
        count_query = count_query.filter(column_favorited_by.contains(ids));
    }

    let count = count_query.execute(&conn)?;

    let articles = query
        .order(column_created_at.desc())
        .offset(std::cmp::max(filters.offset.unwrap_or(0), 0))
        .limit(std::cmp::min(filters.limit.unwrap_or(10), 100))
        .get_results::<(ArticleInDatabase, UserInDatabase)>(&conn)?;

    let is_following = match logged_in_user_id {
        Some(id) => crate::user::get_following(pool, &id)?,

        None => vec![],
    };

    let articles = articles
        .into_iter()
        .map(|(a, u)| {
            let following = is_following.contains(&u.id);
            let tags = crate::tag::get_for_article(pool, &a.id).unwrap_or_else(|_| vec![]);
            a.into_output(u.into_author_output(following), tags, &logged_in_user_id)
        })
        .collect();

    Ok((articles, count))
}

#[derive(Debug)]
pub enum FavoriteOperation {
    Favorite,
    Unfavorite,
}

pub fn manage_favorite(
    pool: &DbPool,
    logged_in_user_id: &uuid::Uuid,
    slug: &str,
    favorite_operation: FavoriteOperation,
) -> Result<ArticleOutput, crate::error::Error> {
    let conn = &pool.get()?;
    let logged_in_user = crate::user::get(pool, &logged_in_user_id)?;
    let (mut article, author) = get_article_and_author_by_slug(pool, &slug)?;
    let mut currently_favorited = article.favorited_by.clone();
    let tags = crate::tag::get_for_article(pool, &article.id)?;
    let is_following = logged_in_user.is_following(&author.id);

    match favorite_operation {
        FavoriteOperation::Favorite => {
            if !currently_favorited.contains(&logged_in_user.id) {
                currently_favorited.push(logged_in_user.id);
            }
        }
        FavoriteOperation::Unfavorite => {
            let index = currently_favorited
                .iter()
                .position(|u| *u == logged_in_user.id);
            if let Some(i) = index {
                currently_favorited.remove(i);
            }
        }
    }

    article.favorited_by = currently_favorited;

    let updated_article = diesel::update(articles_table.find(article.id))
        .set(column_favorited_by.eq(article.favorited_by))
        .get_result::<ArticleInDatabase>(conn)?;

    Ok(updated_article.into_output(
        author.into_author_output(is_following),
        tags,
        &Some(*logged_in_user_id),
    ))
}

fn get_article_and_author_by_slug(
    pool: &DbPool,
    slug: &str,
) -> Result<(ArticleInDatabase, UserInDatabase), crate::error::Error> {
    let conn = &pool.get()?;
    articles_table
        .inner_join(users_table)
        .filter(column_slug.eq(slug))
        .get_result::<(ArticleInDatabase, UserInDatabase)>(conn)
        .map_err(|e| e.into())
}

pub fn get_by_slug(
    pool: &DbPool,
    slug: &str,
    logged_in_user_id: Option<uuid::Uuid>,
) -> Result<ArticleOutput, crate::error::Error> {
    let (article, author) = get_article_and_author_by_slug(pool, &slug)?;

    let mut following = false;
    if let Some(user_id) = logged_in_user_id {
        following = crate::user::get_following(pool, &user_id)?.contains(&author.id)
    }

    let tags = crate::tag::get_for_article(pool, &article.id)?;

    Ok(article.into_output(
        author.into_author_output(following),
        tags,
        &logged_in_user_id,
    ))
}

pub fn update_by_slug(
    pool: &DbPool,
    slug: &str,
    update_article_input: ArticleInput,
    logged_in_user_id: uuid::Uuid,
) -> Result<ArticleOutput, crate::error::Error> {
    let conn = &pool.get()?;
    let (update_article, tags) = update_article_input.into_update_article()?;

    let article = diesel::update(articles_table.filter(column_slug.eq(slug)))
        .set(update_article)
        .get_result::<ArticleInDatabase>(conn)?;

    let author = users_table
        .find(article.author_id)
        .first::<crate::user::UserInDatabase>(conn)?;

    let following = crate::user::get_following(pool, &logged_in_user_id)?.contains(&author.id);

    let _ = crate::tag::delete_all_tags_for_article(pool, article.id)?;

    let _ = crate::tag::insert_tags_for_article(pool, article.id, &tags)?;

    Ok(article.into_output(
        author.into_author_output(following),
        tags,
        &Some(logged_in_user_id),
    ))
}

pub fn comments_by_slug(
    pool: &DbPool,
    slug: &str,
    logged_in_user_id: Option<uuid::Uuid>,
) -> Result<Vec<crate::comment::CommentOutput>, crate::error::Error> {
    let conn = &pool.get()?;
    let article_id = articles_table
        .filter(column_slug.eq(slug))
        .select(column_id)
        .get_result::<uuid::Uuid>(conn)?;

    let logged_in_user = match logged_in_user_id {
        Some(id) => match crate::user::get(pool, &id) {
            Ok(user) => Some(user),
            Err(_) => None,
        },
        None => None,
    };

    crate::comment::get_for_article(pool, article_id, logged_in_user)
}

pub fn delete(
    pool: &DbPool,
    slug: &str,
    logged_in_user_id: &uuid::Uuid,
) -> Result<usize, crate::error::Error> {
    let conn = &pool.get()?;
    let num_deleted = diesel::delete(
        articles_table
            .filter(column_slug.eq(slug))
            .filter(column_author_id.eq(logged_in_user_id)),
    )
    .execute(conn)?;
    Ok(num_deleted)
}

pub fn raw_get_by_slug(
    pool: &DbPool,
    slug: &str,
) -> Result<ArticleInDatabase, crate::error::Error> {
    let conn = &pool.get()?;
    articles_table
        .filter(column_slug.eq(slug))
        .get_result::<ArticleInDatabase>(conn)
        .map_err(|e| e.into())
}
