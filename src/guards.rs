struct AuthGuard;
impl actix_web::guard::Guard for AuthGuard {
    fn check(&self, req: &actix_web::dev::RequestHead) -> bool {
        req.headers()
            .contains_key(actix_web::http::header::AUTHORIZATION)
    }
}
