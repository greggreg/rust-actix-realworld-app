use crate::article;
use crate::comment;
use crate::error;
use crate::jwt;
use crate::tag;
use crate::user;
use crate::DbPool;
use actix_web::error::ResponseError;
use actix_web::{web, HttpResponse};
use article::ArticleInput;
use comment::NewCommentInput;
use futures::future::{err, ok};
use futures::Future;
use user::{NewUserInput, UpdateUserInput, UserLoginInput};

// Another custom type we want to extract from requests
#[derive(Debug, Clone)]
pub struct LoggedInUserId(pub uuid::Uuid);

impl actix_web::FromRequest for LoggedInUserId {
    type Error = actix_web::Error;
    type Future = Result<LoggedInUserId, actix_web::Error>;
    type Config = ();

    fn from_request(req: &actix_web::HttpRequest, _: &mut actix_web::dev::Payload) -> Self::Future {
        let exts = req.extensions();
        let cuid: Option<&LoggedInUserId> = exts.get();
        if let Some(u) = cuid {
            Ok(u.clone())
        } else {
            let b = error::Error::AuthError("No user found in app data.".to_string());
            let a: actix_web::Error = b.into();
            Err(a)
        }
    }
}

#[derive(Debug, Clone)]
pub enum LoggedInUserIdOption {
    None,
    Some(uuid::Uuid),
}

impl actix_web::FromRequest for LoggedInUserIdOption {
    type Error = actix_web::Error;
    type Future = Result<LoggedInUserIdOption, actix_web::Error>;
    type Config = ();

    fn from_request(req: &actix_web::HttpRequest, _: &mut actix_web::dev::Payload) -> Self::Future {
        let exts = req.extensions();
        let cuid: Option<&LoggedInUserId> = exts.get();
        if let Some(u) = cuid {
            Ok(LoggedInUserIdOption::Some(u.0))
        } else {
            Ok(LoggedInUserIdOption::None)
        }
    }
}

impl std::convert::From<LoggedInUserIdOption> for std::option::Option<uuid::Uuid> {
    fn from(input: LoggedInUserIdOption) -> Self {
        match input {
            LoggedInUserIdOption::Some(uuid) => Some(uuid),
            LoggedInUserIdOption::None => None,
        }
    }
}

#[derive(Debug, Deserialize, Serialize)]
pub struct UserWrapper<T> {
    user: T,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct ArticleWrapper<T> {
    article: T,
}

#[derive(Debug, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct ArticlesWrapper<T> {
    articles: T,
    articles_count: usize,
}

#[derive(Debug, Serialize)]
pub struct CommentsWrapper<T> {
    comments: T,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct CommentWrapper<T> {
    comment: T,
}

pub fn handle_create_article(
    input_article: web::Json<ArticleWrapper<ArticleInput>>,
    pool: web::Data<DbPool>,
    logged_in_user_id: LoggedInUserId,
) -> impl Future<Item = actix_web::HttpResponse, Error = error::Error> {
    let article_new = input_article.into_inner().article;
    let x = article::create_article(pool.get_ref(), logged_in_user_id.0, article_new);
    match x {
        Ok(article) => ok(HttpResponse::Ok().json(json!(ArticleWrapper { article }))),
        Err(err) => ok((&err).error_response()),
    }
}

pub fn handle_create_article_comment(
    pool: web::Data<DbPool>,
    slug: web::Path<(String)>,
    input_comment: web::Json<CommentWrapper<NewCommentInput>>,
    logged_in_user_id: LoggedInUserId,
) -> impl Future<Item = actix_web::HttpResponse, Error = error::Error> {
    let comment_new = input_comment.into_inner().comment;
    let new_comment = comment::create(pool.get_ref(), &slug, logged_in_user_id.0, comment_new);
    match new_comment {
        Ok(comment) => ok(HttpResponse::Ok().json(json!(CommentWrapper { comment }))),
        Err(err) => ok((&err).error_response()),
    }
}

pub fn handle_favorite_article(
    pool: web::Data<DbPool>,
    logged_in_user_id: LoggedInUserId,
    slug: web::Path<(String)>,
) -> impl Future<Item = actix_web::HttpResponse, Error = error::Error> {
    match article::manage_favorite(
        pool.get_ref(),
        &logged_in_user_id.0,
        &slug,
        article::FavoriteOperation::Favorite,
    ) {
        Ok(article) => ok(HttpResponse::Ok().json(json!(ArticleWrapper { article }))),
        Err(err) => ok((&err).error_response()),
    }
}

pub fn handle_follow(
    pool: web::Data<DbPool>,
    logged_in_user_id: LoggedInUserId,
    username: web::Path<(String)>,
) -> impl Future<Item = actix_web::HttpResponse, Error = error::Error> {
    match user::manage_follow(
        pool.get_ref(),
        logged_in_user_id.0,
        username.to_string(),
        user::FollowOperation::Follow,
    ) {
        Ok(profile) => ok(HttpResponse::Ok().json(json!({ "profile": profile }))),
        Err(err) => ok((&err).error_response()),
    }
}

pub fn handle_get_article(
    pool: web::Data<DbPool>,
    slug: web::Path<(String)>,
    logged_in_user_id: LoggedInUserIdOption,
) -> impl Future<Item = actix_web::HttpResponse, Error = error::Error> {
    match article::get_by_slug(pool.get_ref(), &slug, logged_in_user_id.into()) {
        Ok(article) => ok(HttpResponse::Ok().json(json!(ArticleWrapper { article }))),
        Err(e) => err(e),
    }
}

pub fn handle_get_article_comments(
    pool: web::Data<DbPool>,
    slug: web::Path<(String)>,
    logged_in_user_id: LoggedInUserIdOption,
) -> impl Future<Item = actix_web::HttpResponse, Error = error::Error> {
    match article::comments_by_slug(pool.get_ref(), &slug, logged_in_user_id.into()) {
        Ok(comments) => ok(HttpResponse::Ok().json(json!(CommentsWrapper { comments }))),
        Err(e) => err(e),
    }
}

pub fn handle_get_articles(
    pool: web::Data<DbPool>,
    search_filters: web::Query<article::SearchFilters>,
    logged_in_user_id: LoggedInUserIdOption,
) -> impl Future<Item = actix_web::HttpResponse, Error = error::Error> {
    match article::get_all(
        pool.get_ref(),
        &search_filters.into_inner(),
        logged_in_user_id.into(),
    ) {
        Ok((articles, articles_count)) => ok(HttpResponse::Ok().json(json!(ArticlesWrapper {
            articles,
            articles_count
        }))),

        Err(e) => err(e),
    }
}

pub fn handle_get_feed(
    pool: web::Data<DbPool>,
    logged_in_user_id: LoggedInUserId,
    mut search_filters: web::Query<article::SearchFilters>,
) -> impl Future<Item = actix_web::HttpResponse, Error = error::Error> {
    match user::get(pool.get_ref(), &logged_in_user_id.0).and_then(|user| {
        search_filters.authors = Some(user.following);
        article::get_all(pool.get_ref(), &search_filters, Some(user.id))
    }) {
        Ok((articles, articles_count)) => ok(HttpResponse::Ok().json(json!(ArticlesWrapper {
            articles,
            articles_count
        }))),

        Err(err) => ok((&err).error_response()),
    }
}

pub fn handle_delete_article(
    pool: web::Data<DbPool>,
    slug: web::Path<(String)>,
    logged_in_user_id: LoggedInUserId,
) -> impl Future<Item = actix_web::HttpResponse, Error = error::Error> {
    match article::delete(&pool, &slug, &logged_in_user_id.0) {
        Ok(_) => ok(HttpResponse::Ok().json("")),
        Err(err) => ok((&err).error_response()),
    }
}

pub fn handle_delete_article_comment(
    pool: web::Data<DbPool>,
    path_parts: web::Path<((String, String))>,
    logged_in_user_id: LoggedInUserId,
) -> impl Future<Item = actix_web::HttpResponse, Error = error::Error> {
    let c_id = uuid::Uuid::parse_str(&path_parts.1);
    let comment_id;
    match c_id {
        Err(e) => return ok((&error::Error::from(uuid::Error::from(e))).error_response()),
        Ok(id) => comment_id = id,
    };
    match comment::delete(&pool, &comment_id, &logged_in_user_id.0) {
        Ok(_) => ok(HttpResponse::Ok().json("")),
        Err(err) => ok((&err).error_response()),
    }
}

pub fn handle_get_profile(
    username: web::Path<(String)>,
    logged_in_user_id: LoggedInUserIdOption,
    pool: web::Data<DbPool>,
) -> impl Future<Item = actix_web::HttpResponse, Error = error::Error> {
    match user::get_profile_for_username(pool.get_ref(), &username, logged_in_user_id.into()) {
        Ok(profile) => ok(HttpResponse::Ok().json(json!({ "profile": profile }))),
        Err(err) => ok((&err).error_response()),
    }
}

pub fn handle_get_tags(
    pool: web::Data<DbPool>,
) -> impl Future<Item = actix_web::HttpResponse, Error = error::Error> {
    match tag::get_all(pool.get_ref()) {
        Ok(tags) => ok(HttpResponse::Ok().json(json!({ "tags": tags }))),
        Err(e) => err(e),
    }
}

pub fn handle_get_user(
    logged_in_user_id: LoggedInUserId,
    pool: web::Data<DbPool>,
    jwt_config: web::Data<jwt::Config>,
) -> impl Future<Item = actix_web::HttpResponse, Error = error::Error> {
    match user::get_user_by_id(pool.get_ref(), jwt_config.get_ref(), logged_in_user_id.0) {
        Ok(user) => ok(HttpResponse::Ok().json(json!(UserWrapper { user }))),
        Err(err) => ok((&err).error_response()),
    }
}

pub fn handle_login(
    input_user: web::Json<UserWrapper<UserLoginInput>>,
    pool: web::Data<DbPool>,
    jwt_config: web::Data<jwt::Config>,
) -> impl Future<Item = actix_web::HttpResponse, Error = error::Error> {
    let credentials = input_user.into_inner().user;
    match user::login(pool.get_ref(), jwt_config.get_ref(), credentials) {
        Ok(user) => ok(HttpResponse::Ok().json(json!(UserWrapper { user }))),
        Err(err) => ok((&err).error_response()),
    }
}

pub fn handle_register(
    input_user: web::Json<UserWrapper<NewUserInput>>,
    pool: web::Data<DbPool>,
    jwt_config: web::Data<jwt::Config>,
) -> impl Future<Item = actix_web::HttpResponse, Error = error::Error> {
    let user_new = input_user.into_inner().user;
    match user::create_user(pool.get_ref(), jwt_config.get_ref(), user_new) {
        Ok(user) => ok(HttpResponse::Ok().json(json!(UserWrapper { user }))),
        Err(err) => ok((&err).error_response()),
    }
}

pub fn handle_unfavorite_article(
    pool: web::Data<DbPool>,
    logged_in_user_id: LoggedInUserId,
    slug: web::Path<(String)>,
) -> impl Future<Item = actix_web::HttpResponse, Error = error::Error> {
    match article::manage_favorite(
        pool.get_ref(),
        &logged_in_user_id.0,
        &slug,
        article::FavoriteOperation::Unfavorite,
    ) {
        Ok(article) => ok(HttpResponse::Ok().json(json!(ArticleWrapper { article }))),
        Err(err) => ok((&err).error_response()),
    }
}

pub fn handle_unfollow(
    pool: web::Data<DbPool>,
    logged_in_user_id: LoggedInUserId,
    username: web::Path<(String)>,
) -> impl Future<Item = actix_web::HttpResponse, Error = error::Error> {
    match user::manage_follow(
        pool.get_ref(),
        logged_in_user_id.0,
        username.to_string(),
        user::FollowOperation::Unfollow,
    ) {
        Ok(profile) => ok(HttpResponse::Ok().json(json!({ "profile": profile }))),
        Err(err) => ok((&err).error_response()),
    }
}

pub fn handle_update_article(
    input_article: web::Json<ArticleWrapper<ArticleInput>>,
    pool: web::Data<DbPool>,
    logged_in_user_id: LoggedInUserId,
    slug: web::Path<(String)>,
) -> impl Future<Item = actix_web::HttpResponse, Error = error::Error> {
    let article_input = input_article.into_inner().article;
    let article =
        article::update_by_slug(pool.get_ref(), &slug, article_input, logged_in_user_id.0);
    match article {
        Ok(article) => ok(HttpResponse::Ok().json(json!(ArticleWrapper { article }))),
        Err(err) => ok((&err).error_response()),
    }
}

pub fn handle_update_user(
    input_user: web::Json<UserWrapper<UpdateUserInput>>,
    pool: web::Data<DbPool>,
    jwt_config: web::Data<jwt::Config>,
    logged_in_user_id: LoggedInUserId,
) -> impl Future<Item = actix_web::HttpResponse, Error = error::Error> {
    let user_new = input_user.into_inner().user;
    let updated_user = user::update_user(
        pool.get_ref(),
        jwt_config.get_ref(),
        logged_in_user_id.0,
        user_new,
    );
    match updated_user {
        Ok(user) => ok(HttpResponse::Ok().json(json!(UserWrapper { user }))),
        Err(err) => ok((&err).error_response()),
    }
}
