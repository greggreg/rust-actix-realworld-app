// https://github.com/emk/rust-musl-builder/issues/69.
extern crate openssl;
#[macro_use]
extern crate diesel;
#[macro_use]
extern crate failure;
#[macro_use]
extern crate serde_derive;
#[macro_use]
extern crate serde_json;
#[macro_use]
extern crate validator_derive;
mod api;
mod article;
mod comment;
mod error;
mod jwt;
mod middleware;
mod schema;
mod tag;
mod user;
use actix_cors::Cors;
use actix_files::NamedFile;
use actix_web::middleware::Logger;
use actix_web::{web, App, HttpServer};
use diesel::prelude::PgConnection;
use diesel::r2d2::ConnectionManager;
use env_logger;
use futures::future::ok;
use futures::Future;

// Name of the application from Corgo.toml
const APP_NAME: &str = env!("CARGO_PKG_NAME");

// We must declare a DbPool type so we can extract it from requests
pub type DbPool = r2d2::Pool<ConnectionManager<PgConnection>>;

fn main() -> std::io::Result<()> {
    // Setup the logger
    std::env::set_var("RUST_LOG", "actix_web=info");
    env_logger::builder()
        .default_format_module_path(false)
        .init();

    // Load the database connection from an environment variable
    let database_url = dotenv::var("DATABASE_URL")
        .expect("DATABASE_URL must be set to a postgres connection spec");

    // Super secret key to sign JWTs with
    let jwt_secret = jwt::Config {
        secret: dotenv::var("JWT_SECRET")
            .expect("JWT_SECRET must be set")
            .to_string(),
    };

    // Initialize the connection manager for the database
    let connection_manager = ConnectionManager::<PgConnection>::new(database_url);

    // Build a pool of database connections
    let db_pool = r2d2::Pool::builder()
        .build(connection_manager)
        .expect("could not create database pool");

    // Build the HttpServer configuration
    let mut server = HttpServer::new(move || {
        App::new()
            .data(db_pool.clone())
            .data(web::JsonConfig::default().limit(4096))
            .data(jwt_secret.clone())
            .wrap(middleware::api_auth::JwtAuth)
            .wrap(Logger::new("%r %s %Dms"))
            .wrap(Cors::new())
            .wrap(middleware::normalize_slashes::NormalizeSlashes)
            .service(
                web::scope("/api")
                    .service(
                        web::scope("/users")
                            .route("", web::post().to_async(api::handle_register))
                            .route("/login", web::post().to_async(api::handle_login)),
                    )
                    .service(
                        web::scope("/user")
                            .route("", web::get().to_async(api::handle_get_user))
                            .route("", web::put().to_async(api::handle_update_user)),
                    )
                    .route("/tags", web::get().to_async(api::handle_get_tags))
                    .service(
                        web::scope("/profiles/{username}")
                            .route("", web::get().to_async(api::handle_get_profile))
                            .route("/follow", web::post().to_async(api::handle_follow))
                            .route("/follow", web::delete().to_async(api::handle_unfollow)),
                    )
                    .service(
                        web::scope("/articles")
                            .route("", web::get().to_async(api::handle_get_articles))
                            .route("/feed", web::get().to_async(api::handle_get_feed))
                            .service(
                                web::scope("/{slug}")
                                    .route(
                                        "/comments",
                                        web::get().to_async(api::handle_get_article_comments),
                                    )
                                    .route(
                                        "/comments",
                                        web::post().to_async(api::handle_create_article_comment),
                                    )
                                    .route(
                                        "/comments/{id}",
                                        web::delete().to_async(api::handle_delete_article_comment),
                                    )
                                    .route(
                                        "/favorite",
                                        web::post().to_async(api::handle_favorite_article),
                                    )
                                    .route(
                                        "/favorite",
                                        web::delete().to_async(api::handle_unfavorite_article),
                                    )
                                    .route("", web::get().to_async(api::handle_get_article))
                                    .route("", web::put().to_async(api::handle_update_article))
                                    .route("", web::delete().to_async(api::handle_delete_article)),
                            )
                            .route("", web::post().to_async(api::handle_create_article)),
                    ),
            )
            .service(web::resource("").route(web::get().to_async(handle_home)))
    });

    // In order to re-compile when files change we setup listenfd
    // if we started using systemfd we need to get the socket from listenfd
    // otherwise we bind to a socket ourselves
    let mut listenfd = listenfd::ListenFd::from_env();
    server = if let Some(listener) = listenfd.take_tcp_listener(0).unwrap() {
        println!(
            "{} running at {} ",
            APP_NAME,
            listener.local_addr().unwrap()
        );
        server.listen(listener).unwrap()
    } else {
        println!("{} running at http://0.0.0.0:8000/ ", APP_NAME);
        server.bind("0.0.0.0:8000").unwrap()
    };

    // run the server
    server.run()
}

fn handle_home() -> impl Future<Item = actix_files::NamedFile, Error = error::Error> {
    ok(NamedFile::open("./static/index.html").unwrap())
}
