use crate::schema::comments;
use crate::schema::comments::dsl::{
    article_id as column_article_id, author_id as column_author_id, comments as comments_table,
    id as column_id,
};
use crate::schema::users::dsl::users as users_table;
use crate::user::UserInDatabase;
use crate::DbPool;
use diesel::ExpressionMethods;
use diesel::QueryDsl;
use diesel::RunQueryDsl;
use validator::Validate;

/*
 * INPUT STRUCTS
 */
#[derive(Debug, Deserialize, Validate)]
pub struct NewCommentInput {
    #[validate(length(min = "1", message = "cannot be empty"))]
    #[serde(default)]
    body: String,
}

impl NewCommentInput {
    fn into_new_comment(
        self,
        article_id: &uuid::Uuid,
        author_id: &uuid::Uuid,
    ) -> Result<NewComment, crate::error::Error> {
        self.validate()?;
        Ok(NewComment {
            article_id: *article_id,
            author_id: *author_id,
            body: self.body,
        })
    }
}

/*
 * OUTPUT STRUCTS
 */
#[derive(Debug, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct CommentOutput {
    author: crate::user::AuthorOutput,
    body: String,
    created_at: chrono::DateTime<chrono::Utc>,
    id: uuid::Uuid,
    updated_at: chrono::DateTime<chrono::Utc>,
}

/*
 * APPLICATION STRUCTS
 */
#[derive(Debug, Insertable)]
#[table_name = "comments"]
struct NewComment {
    pub article_id: uuid::Uuid,
    pub author_id: uuid::Uuid,
    pub body: String,
}

#[derive(Debug, Insertable, Queryable, Serialize)]
#[table_name = "comments"]
pub struct CommentInDatabase {
    pub article_id: uuid::Uuid,
    pub author_id: uuid::Uuid,
    pub body: String,
    pub created_at: chrono::DateTime<chrono::Utc>,
    pub id: uuid::Uuid,
    pub updated_at: chrono::DateTime<chrono::Utc>,
}

impl CommentInDatabase {
    fn into_output(self, author: crate::user::AuthorOutput) -> CommentOutput {
        CommentOutput {
            author,
            body: self.body,
            created_at: self.created_at,
            id: self.id,
            updated_at: self.updated_at,
        }
    }
}

pub fn get_for_article(
    pool: &DbPool,
    article_id: uuid::Uuid,
    logged_in_user: Option<UserInDatabase>,
) -> Result<Vec<crate::comment::CommentOutput>, crate::error::Error> {
    let conn = &pool.get()?;
    let comments: Vec<(CommentInDatabase, UserInDatabase)> = comments_table
        .filter(column_article_id.eq(article_id))
        .inner_join(users_table)
        .get_results(conn)?;

    Ok(comments
        .into_iter()
        .map(move |(comment, user)| {
            let following = match &logged_in_user {
                Some(u) => u.is_following(&user.id),
                None => false,
            };
            let author = user.into_author_output(following);
            comment.into_output(author)
        })
        .collect())
}

pub fn create(
    pool: &DbPool,
    article_slug: &str,
    logged_in_user_id: uuid::Uuid,
    new_comment_input: NewCommentInput,
) -> Result<CommentOutput, crate::error::Error> {
    let conn = &pool.get()?;
    let article = crate::article::raw_get_by_slug(pool, article_slug)?;
    let new_comment = new_comment_input.into_new_comment(&article.id, &logged_in_user_id)?;
    let user = crate::user::get(pool, &logged_in_user_id)?;
    let following = user.is_following(&user.id);

    let comment = diesel::insert_into(comments_table)
        .values(new_comment)
        .get_result::<CommentInDatabase>(conn)?;

    let author = user.into_author_output(following);
    Ok(comment.into_output(author))
}

pub fn delete(
    pool: &DbPool,
    comment_id: &uuid::Uuid,
    logged_in_user_id: &uuid::Uuid,
) -> Result<usize, crate::error::Error> {
    let conn = &pool.get()?;
    let num_deleted = diesel::delete(
        comments_table
            .filter(column_id.eq(comment_id))
            .filter(column_author_id.eq(logged_in_user_id)),
    )
    .execute(conn)?;
    Ok(num_deleted)
}
